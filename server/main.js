var io = require("socket.io")();
var request = require("request");
//var mainUrl = "http://localhost:8080/imagehoster/index.php/getuser";
var mainUrl = "http://isteinvids.co.uk/imagehoster/index.php/getuser";

console.log("Starting server");

function getVerifyUserFromRand(token, callback){
	request({ url: mainUrl+"?token="+decodeURIComponent(token) }, function (error, response, body) {
		console.log(token);
		if (!error && response.statusCode === 200 && body) {
			callback(body);
		}
	});
//	return "user"+Math.round((Math.random() * 10000)); // fix later
}

var connections = {};

io.on("connection", function(socket){
	socket.username = false;
	socket.talkingTo = {};
	function findOnlinePeople(){
		var cons = {};
		for(var key in connections){
			var obj = connections[key];
			if(obj.username && obj.publicKey && socket.username != obj.username){
				cons[obj.username] = obj.publicKey;
			}
		}
		return cons;
	}
	socket.sendonlinepeople = function(){
		socket.emit("sendonlinepeople", {keys:findOnlinePeople()});
	}
	socket.on("handshake", function(data){
		getVerifyUserFromRand(data.userId, function(user){
			socket.username = user;
			console.log(socket.username+" has connected");
			if(socket.username !== false){
				connections[socket.username] = socket;
			}
			socket.publicKey = data.publicKey;
			for(var key in connections){
				connections[key].sendonlinepeople();
				connections[key].emit("personConnect", socket.username);
			}
		});
//		console.log(data.publicKey, data.userId, data.talkingTo);
	});
	socket.on("sendonlinepeople", function(data){
		socket.sendonlinepeople();
	});
	socket.on("sendkey", function(data){
		var usr = data.username;
		if(connections[usr] && connections[usr].publicKey){
			connections[usr].emit("sendkey", {username:socket.username,key:data.key});
		} else {
			socket.sendonlinepeople();
		}
	});
	socket.on("sendmsg", function(data){
		var usr = data.username;
		if(connections[usr] && connections[usr].publicKey){
			connections[usr].emit("sendmsg", {username:socket.username,msg:data.msg});
		} else {
			socket.sendonlinepeople();
		}
	});
	socket.on("disconnect", function(data){
		delete(connections[socket.username]);
		for(var key in connections){
			connections[key].sendonlinepeople();
			connections[key].emit("personDisconnect", socket.username);
		}
	});
});

io.listen(4445);