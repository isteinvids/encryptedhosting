<?php

class Users {
	
	public function __construct($db) {
		$this->db = $db;
	}

	public function str_to_password($password){
		$options = [
			'cost' => 12,
			'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
		];
		return password_hash($password, PASSWORD_BCRYPT, $options);
	}
	
	public function verify_password($encrypted, $raw){
		return password_verify($raw, $encrypted);
	}
	
	function validate_username($str) {
		$allowed = array(".", "-", "_"," ");
		if(ctype_alnum(str_replace($allowed, '', $str))){
			return true;
		}
		return false;
	}
	function validate_email($email){
		return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
	}
	
	public function getUserFromToken($token){
		$st = $this->db->prepare("SELECT * FROM users WHERE token={$this->db->quote($token)}"); // and token_date is no more than 10 minutes ago
		$st->execute();
		$cok = $st->fetch();
		
		if($cok !== false){
			$st = $this->db->prepare("UPDATE users SET token='',token_updated=NULL WHERE id={$this->db->quote($cok['id'])}");
			$st->execute();
		}
		
		return $cok;
	}
	
	public function register($data){
		if($this->validate_username($data['username']) && $this->validate_email($data['email'])){
			if($data['password1'] === $data['password2']){
				$st = $this->db->prepare("SELECT * FROM users WHERE lower(username)={$this->db->quote(strtolower($data['username']))}");
				$st->execute();
				$exists_username = $st->fetchAll();
				
				$st = $this->db->prepare("SELECT * FROM users WHERE lower(email)={$this->db->quote(strtolower($data['email']))}");
				$st->execute();
				$exists_email = $st->fetchAll();
				
				if(empty($exists_username)){
					if(empty($exists_email)){
						$encrypted_password = $this->str_to_password($data['password1']);
						$st = $this->db->prepare("INSERT INTO users (username,email,password,is_banned,is_admin,is_hidden,create_date) VALUES ".
										   "({$this->db->quote($data['username'])}, {$this->db->quote($data['email'])}, {$this->db->quote($encrypted_password)}, '0', '0', '0', NOW());");
						$st->execute();
						return true;
					} else {
						return "register_email_exists";
					}
				} else {
					return "register_username_exists";
				}
			} else {
				return "passwords_no_match";
			}
		} else {
			return "invalid_username_or_email";
		}
	}
	
	private function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	public function generateRand($userid){
		$rand = $this->generateRandomString(16);
		$st = $this->db->prepare("UPDATE users SET token={$this->db->quote($rand)}, token_updated=NOW() WHERE id={$this->db->quote($userid)}");
		$st->execute();
		return $rand;
	}
	
	public function login($username, $password){
		$st = $this->db->prepare("SELECT * FROM users WHERE lower(username)={$this->db->quote(strtolower($username))} OR lower(email)={$this->db->quote(strtolower($username))}");
		$st->execute();
		$user = $st->fetch();
		if($user === false){
			return "bad_login";
		}
		if($this->verify_password($user['password'], $password)){
			$st = $this->db->prepare("UPDATE users SET lastused_date=NOW() WHERE id={$this->db->quote($user['id'])}");
			$st->execute();
			$_SESSION['user'] = $user;
			session_regenerate_id(false);
			return true;
		}
		return "bad_login";
	}
	
	public function getCookieAndSetUsed($cookie){
		$st = $this->db->prepare("SELECT * FROM cookies WHERE cookie={$this->db->quote($cookie)} AND used_date IS NULL");
		$st->execute();
		$cok = $st->fetch();
		
		if($cok !== false){
			$st = $this->db->prepare("UPDATE cookies SET used_date=NOW() WHERE id={$this->db->quote($cok['id'])}");
			$st->execute();
		}
		
		return $cok;
	}
	
	public function addNewCookie($userid){
		$this->relogin($userid);
		$cookie = $this->generateRandomString(63);
		$st = $this->db->prepare("INSERT INTO cookies (user_id,cookie,create_date) VALUES ({$this->db->quote($userid)}, {$this->db->quote($cookie)}, NOW())");
		$st->execute();
		setcookie("user", $cookie, time()+(60*60*24*14));
	}
	
	public function setCookiesUsed(){
		if(isset($_COOKIE["user"])){
			$cookie = $_COOKIE["user"];
			$cookie_item = $this->getCookieAndSetUsed($cookie);
			setcookie("user", "", time()-3600);
		}
	}
	
	public function relogin($userid){
		$st = $this->db->prepare("SELECT * FROM users WHERE id={$this->db->quote($userid)}");
		$st->execute();
		$user = $st->fetch();
		if(user == false){
			unset($_SESSION['user']);
		} else {
			$_SESSION['user'] = $user;
		}
	}
	
	public function updateSession(){
		if(isset($_SESSION['user']) && $_SESSION['user'] !== false){
			$this->relogin($_SESSION['user']['id']);
		} else {
			if(isset($_COOKIE["user"])){
				$cookie = $_COOKIE["user"];
				
				$cookie_item = $this->getCookieAndSetUsed($cookie);
				
				if($cookie_item !== false){
					$this->addNewCookie($cookie_item['user_id']);
				} else {
					setcookie("user", "", time()-3600);
				}
			}
		}
	}
}