<?php

class Images {
	
	public function __construct($db) {
		$this->db = $db;
	}
	
	private function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	public function saveImage($encrypted, $md5, $userid) {
		$str = $this->generateRandomString(16);
		$st = $this->db->prepare("INSERT INTO images (encrypted, rand, md5, user_id, create_date) VALUES ({$this->db->quote($encrypted)}, {$this->db->quote($str)}, {$this->db->quote($md5)}, {$this->db->quote($userid)}, NOW());");
		$st->execute();
		return $str;
	}
	
	public function getImage($rand, $md5=false){
		$st = $this->db->prepare("SELECT * FROM images WHERE rand={$this->db->quote($rand)} ".($md5!==false?"AND md5=".$this->db->quote($md5):""));
		$st->execute();
		$cok = $st->fetch();
		return $cok;
	}
	
	public function addComment($imageid, $encrypted, $md5, $userid){
		$image = $this->getImage($imageid, $md5);
		if(strlen($encrypted) <= 216){
		if($image !== false){
			$st = $this->db->prepare("INSERT INTO comments (image_id, user_id, message, create_date) VALUES ({$this->db->quote($image['id'])}, {$this->db->quote($userid)}, {$this->db->quote($encrypted)}, NOW())");
			$st->execute();
		}
		}
	}
	
	public function getCommentsForMD5($rand, $md5=false, $lastid=false){
		$image = $this->getImage($rand, $md5);
		if($image !== false){
			$st = $this->db->prepare("SELECT comments.*, users.username FROM comments ".
									 "LEFT OUTER JOIN users ON comments.user_id=users.id ".
									 "WHERE image_id={$this->db->quote($image['id'])} ".($lastid!==false?"AND comments.id > ".$this->db->quote($lastid):""));
			$st->execute();
			$coks = $st->fetchAll();
			return $coks;
		}
		return false;
	}
}