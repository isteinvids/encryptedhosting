<?php

	error_reporting(E_ERROR | E_WARNING | E_PARSE);

	require 'libraries/smarty/Autoloader.php';
	require 'classes/Users.php';
	require 'classes/Images.php';

	Smarty_Autoloader::register();

	$config = array(
		"host"=>"localhost",
		"username"=>"root",
		"password"=>"",
		"dbname"=>"imagehoster",
		"port"=>3306
	);
//	$config = array(
//		"host"=>"localhost",
//		"username"=>"website",
//		"password"=>"amyra3y4e",
//		"dbname"=>"isteinvids_forum",
//		"port"=>3306
//	);


	$path = explode("/", substr($_SERVER['PATH_INFO'], 1));

	$view = new Smarty();
	$view->setTemplateDir('templates/');
	$view->setCompileDir('cache/smarty/compiled/');
	$view->setCacheDir('cache/smarty/cache/');
	$view->compile_check = true;
	$view->force_compile = true;

	$db = new PDO("mysql:host={$config['host']};dbname={$config['dbname']};port={$config['port']}", $config['username'], $config['password']);

	$localize = function($text){
		return $text;
	};

	function error404(){
		header("HTTP/1.0 404 Not Found");
		var_dump("error 404");
		exit;
	}

	if(!file_exists("pages/$path[0].php")){
		error404();
	}

	$basehref = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']."/";

	$view->assign("basehref", $basehref);

	$display = function($template, $main='main')use($view){
		$view->display("extends:{$main}.tpl|{$template}.tpl");
	};
	$redirect = function($par)use($basehref){
		header("Location: {$basehref}{$par}");
	};

	session_start();

	$_users = new Users($db);
	$_images = new Images($db);

	$_users->updateSession();

	$is_staff = isset($_SESSION['user']) && ($_SESSION['user']['is_staff'] == 1 || $_SESSION['user']['is_admin'] == 1);

	$view->assign("is_staff", $is_staff);

	require("pages/$path[0].php");
	return;

//	var_dump($path);