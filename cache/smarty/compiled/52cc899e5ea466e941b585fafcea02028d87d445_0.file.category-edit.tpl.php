<?php
/* Smarty version 3.1.28, created on 2016-01-16 15:34:26
  from "C:\Users\Emir\Desktop\iKingsSGC\templates\admin\category-edit.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_569a54f23c0091_11778378',
  'file_dependency' => 
  array (
    '52cc899e5ea466e941b585fafcea02028d87d445' => 
    array (
      0 => 'C:\\Users\\Emir\\Desktop\\iKingsSGC\\templates\\admin\\category-edit.tpl',
      1 => 1452954689,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_569a54f23c0091_11778378 ($_smarty_tpl) {
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, false);
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "content", array (
  0 => 'block_18633569a54f235cb16_19416699',
  1 => false,
  3 => 0,
  2 => 0,
));
}
/* {block 'content'}  file:admin/category-edit.tpl */
function block_18633569a54f235cb16_19416699($_smarty_tpl, $_blockParentStack) {
?>

lalalala category <?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>

<br>
<form method="post">
	Parent: <br>
	<select name="parent">
		<option value=""></option>
		<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat_0_saved_item = isset($_smarty_tpl->tpl_vars['cat']) ? $_smarty_tpl->tpl_vars['cat'] : false;
$_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable();
$__foreach_cat_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->value) {
$__foreach_cat_0_saved_local_item = $_smarty_tpl->tpl_vars['cat'];
?>
			<?php if ($_smarty_tpl->tpl_vars['cat']->value['id'] != $_smarty_tpl->tpl_vars['category']->value['id']) {?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['cat']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['category']->value['parent'] == $_smarty_tpl->tpl_vars['cat']->value['id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</option>
				<?php
$_from = $_smarty_tpl->tpl_vars['cat']->value['children'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat2_1_saved_item = isset($_smarty_tpl->tpl_vars['cat2']) ? $_smarty_tpl->tpl_vars['cat2'] : false;
$_smarty_tpl->tpl_vars['cat2'] = new Smarty_Variable();
$__foreach_cat2_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat2_1_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat2']->value) {
$__foreach_cat2_1_saved_local_item = $_smarty_tpl->tpl_vars['cat2'];
?>
					<?php if ($_smarty_tpl->tpl_vars['cat2']->value['id'] != $_smarty_tpl->tpl_vars['category']->value['id']) {?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['cat2']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['category']->value['parent'] == $_smarty_tpl->tpl_vars['cat2']->value['id']) {?>selected<?php }?>>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['cat2']->value['name'];?>
</option>
					<?php }?>
				<?php
$_smarty_tpl->tpl_vars['cat2'] = $__foreach_cat2_1_saved_local_item;
}
}
if ($__foreach_cat2_1_saved_item) {
$_smarty_tpl->tpl_vars['cat2'] = $__foreach_cat2_1_saved_item;
}
?>
			<?php }?>
		<?php
$_smarty_tpl->tpl_vars['cat'] = $__foreach_cat_0_saved_local_item;
}
}
if ($__foreach_cat_0_saved_item) {
$_smarty_tpl->tpl_vars['cat'] = $__foreach_cat_0_saved_item;
}
?>
	</select>
	<br><br>
	Name: <br>
	<input type="text" name="name" value="<?php echo $_smarty_tpl->tpl_vars['category']->value['name'];?>
"><br><br>
	Staff only can post here: <input type="checkbox" name="staff_only_can_post" <?php if ($_smarty_tpl->tpl_vars['category']->value['staff_only_can_post'] == 1) {?>checked<?php }?>><br><br>
	Hidden for normal people: <input type="checkbox" name="hidden_for_nonstaff" <?php if ($_smarty_tpl->tpl_vars['category']->value['hidden_for_nonstaff'] == 1) {?>checked<?php }?>><br><br>
	<button>OK</button>
</form>
<?php
}
/* {/block 'content'} */
}
