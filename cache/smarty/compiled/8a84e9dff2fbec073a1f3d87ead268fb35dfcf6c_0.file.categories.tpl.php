<?php
/* Smarty version 3.1.28, created on 2016-01-17 20:56:30
  from "C:\Users\Emir\Desktop\iKingsSGC\templates\admin\categories.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_569bf1ee325da7_93443593',
  'file_dependency' => 
  array (
    '8a84e9dff2fbec073a1f3d87ead268fb35dfcf6c' => 
    array (
      0 => 'C:\\Users\\Emir\\Desktop\\iKingsSGC\\templates\\admin\\categories.tpl',
      1 => 1452579329,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_569bf1ee325da7_93443593 ($_smarty_tpl) {
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, false);
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "content", array (
  0 => 'block_20500569bf1ee2afed0_66563367',
  1 => false,
  3 => 0,
  2 => 0,
));
}
/* {block 'content'}  file:admin/categories.tpl */
function block_20500569bf1ee2afed0_66563367($_smarty_tpl, $_blockParentStack) {
?>

<style>
	.sortable {
		list-style-type: none;
		margin: 0;
		padding: 0;
	}
	.sortable li {
		margin: 0;
		padding: 0;
	}
	.recorditem {
		padding: 8px 10px;
		border: solid gray 1px;
		margin-top: -1px;
		background: rgb(237,237,237);
		background: -moz-linear-gradient(top, rgba(237,237,237,1) 0%, rgba(201,201,201,1) 100%);
		background: -webkit-linear-gradient(top, rgba(237,237,237,1) 0%,rgba(201,201,201,1) 100%);
		background: linear-gradient(to bottom, rgba(237,237,237,1) 0%,rgba(201,201,201,1) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ededed', endColorstr='#c9c9c9',GradientType=0 );
	}
</style>
list of categories:
<br><br><br>
<ul class="sortable">
	<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat1_0_saved_item = isset($_smarty_tpl->tpl_vars['cat1']) ? $_smarty_tpl->tpl_vars['cat1'] : false;
$_smarty_tpl->tpl_vars['cat1'] = new Smarty_Variable();
$__foreach_cat1_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat1_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat1']->value) {
$__foreach_cat1_0_saved_local_item = $_smarty_tpl->tpl_vars['cat1'];
?>
	<li class="level0">
		<div class="recorditem" catid="<?php echo $_smarty_tpl->tpl_vars['cat1']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['cat1']->value['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['cat1']->value['name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['cat1']->value['parent'];?>
 - <?php echo $_smarty_tpl->tpl_vars['cat1']->value['create_date'];?>
 - <a href="admin/categories?add=<?php echo $_smarty_tpl->tpl_vars['cat1']->value['id'];?>
">Add</a> - <a href="admin/categories?edit=<?php echo $_smarty_tpl->tpl_vars['cat1']->value['id'];?>
">Edit</a> - <a href="admin/categories?delete=<?php echo $_smarty_tpl->tpl_vars['cat1']->value['id'];?>
">Delete</a></div>
		<ul class="sortable">
		<?php
$_from = $_smarty_tpl->tpl_vars['cat1']->value['children'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat2_1_saved_item = isset($_smarty_tpl->tpl_vars['cat2']) ? $_smarty_tpl->tpl_vars['cat2'] : false;
$_smarty_tpl->tpl_vars['cat2'] = new Smarty_Variable();
$__foreach_cat2_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat2_1_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat2']->value) {
$__foreach_cat2_1_saved_local_item = $_smarty_tpl->tpl_vars['cat2'];
?>
			<li class="level1" style="padding-left: 20px;">
				<div class="recorditem" catid="<?php echo $_smarty_tpl->tpl_vars['cat2']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['cat2']->value['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['cat2']->value['name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['cat2']->value['parent'];?>
 - <?php echo $_smarty_tpl->tpl_vars['cat2']->value['create_date'];?>
 - <a href="admin/categories?add=<?php echo $_smarty_tpl->tpl_vars['cat2']->value['id'];?>
">Add</a> - <a href="admin/categories?edit=<?php echo $_smarty_tpl->tpl_vars['cat2']->value['id'];?>
">Edit</a> - <a href="admin/categories?delete=<?php echo $_smarty_tpl->tpl_vars['cat2']->value['id'];?>
">Delete</a></div>
				<ul class="sortable">
					<?php
$_from = $_smarty_tpl->tpl_vars['cat2']->value['children'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat3_2_saved_item = isset($_smarty_tpl->tpl_vars['cat3']) ? $_smarty_tpl->tpl_vars['cat3'] : false;
$_smarty_tpl->tpl_vars['cat3'] = new Smarty_Variable();
$__foreach_cat3_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat3_2_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat3']->value) {
$__foreach_cat3_2_saved_local_item = $_smarty_tpl->tpl_vars['cat3'];
?>
					<li class="level2" style="padding-left: 40px;">
						<div class="recorditem" catid="<?php echo $_smarty_tpl->tpl_vars['cat3']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['cat3']->value['id'];?>
 - <?php echo $_smarty_tpl->tpl_vars['cat3']->value['name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['cat3']->value['parent'];?>
 - <?php echo $_smarty_tpl->tpl_vars['cat3']->value['create_date'];?>
 - <a href="admin/categories?edit=<?php echo $_smarty_tpl->tpl_vars['cat3']->value['id'];?>
">Edit</a> - <a href="admin/categories?delete=<?php echo $_smarty_tpl->tpl_vars['cat3']->value['id'];?>
">Delete</a></div>
					</li>
					<?php
$_smarty_tpl->tpl_vars['cat3'] = $__foreach_cat3_2_saved_local_item;
}
}
if ($__foreach_cat3_2_saved_item) {
$_smarty_tpl->tpl_vars['cat3'] = $__foreach_cat3_2_saved_item;
}
?>
				</ul>
			</li>
		<?php
$_smarty_tpl->tpl_vars['cat2'] = $__foreach_cat2_1_saved_local_item;
}
}
if ($__foreach_cat2_1_saved_item) {
$_smarty_tpl->tpl_vars['cat2'] = $__foreach_cat2_1_saved_item;
}
?>
		</ul>
	</li>
	<?php
$_smarty_tpl->tpl_vars['cat1'] = $__foreach_cat1_0_saved_local_item;
}
}
if ($__foreach_cat1_0_saved_item) {
$_smarty_tpl->tpl_vars['cat1'] = $__foreach_cat1_0_saved_item;
}
?>
</ul>
<br><br><br>
<a href="admin/categories?add">Add</a>
<?php echo '<script'; ?>
>
	function findlevels(){
		var sorting = { };
		
		$(".level0").each(function(){
			var lvl0id = "_"+$(this).children(".recorditem").attr("catid");
			sorting[lvl0id] = { };
			
			$(this).find(".level1").each(function(){
				var lvl1id = "_"+$(this).children(".recorditem").attr("catid");
				sorting[lvl0id][lvl1id] = [];
				
				$(this).find(".level2").each(function(){
					var lvl2id = "_"+$(this).children(".recorditem").attr("catid");
					sorting[lvl0id][lvl1id].push(lvl2id);// = lvl2id;
				});
				
			});
			
		});

		$.post(location.href, { "sorting" : JSON.stringify(sorting) });
		return sorting;
	}
	$( ".sortable" ).sortable({
        stop:  function (event, ui) {
			findlevels();
        }
	});
	$( ".sortable" ).disableSelection();
<?php echo '</script'; ?>
>
<?php
}
/* {/block 'content'} */
}
