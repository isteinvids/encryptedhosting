<?php
/* Smarty version 3.1.28, created on 2016-03-13 20:46:07
  from "E:\PHP\encryptedimagehosting\templates\messages.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56e5c37f05f3c9_09788637',
  'file_dependency' => 
  array (
    'cf4cb61e9a62615591f8c1b95d2d8b785195291d' => 
    array (
      0 => 'E:\\PHP\\encryptedimagehosting\\templates\\messages.tpl',
      1 => 1457679605,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56e5c37f05f3c9_09788637 ($_smarty_tpl) {
?>

<html>
	<head>
		<title>IM</title>
		<?php echo '<script'; ?>
 src="assets/js/jsencrypt.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.4/angular-material.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="https://cdn.socket.io/socket.io-1.4.5.js"><?php echo '</script'; ?>
>
		
		<?php echo '<script'; ?>
 src="assets/js/crypto/core-min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/crypto/md5-min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/crypto/evpkdf-min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/crypto/enc-base64-min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/crypto/cipher-core-min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/crypto/aes-min.js"><?php echo '</script'; ?>
>
	</head>
	<body>
		<style>
			a {
				color: blue;
				text-decoration: underline;
				cursor: pointer;
			}
		</style>
		<div ng-controller="mainController" ng-app="EncryptedMessaging">
			<div user="UX86Nmf1HP"></div>
			<div ng-repeat="(key, value) in messages[talkingTo]"><b ng-if="value.from">{{value.from}}: </b> {{value.msg}}</div>
			<form id="chatform" ng-submit="onChatInput(msg)">
				<input type="text" ng-model="msg">
				<button>ENTER</button>
			</form>
			<br>
			<div ng-repeat="(key, value) in keys"><a ng-click="startTalkWith(key, true)">{{key}}</a> | </div>
		</div>
		<?php echo '<script'; ?>
>var userInfo = <?php echo $_smarty_tpl->tpl_vars['info']->value;?>
;<?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/im.js"><?php echo '</script'; ?>
>
	</body>
</html>
<?php }
}
