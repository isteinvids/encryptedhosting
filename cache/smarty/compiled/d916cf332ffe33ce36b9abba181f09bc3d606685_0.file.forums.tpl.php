<?php
/* Smarty version 3.1.28, created on 2016-02-27 22:14:32
  from "C:\Users\Emir\Desktop\iKingsSGC\templates\forums.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56d211b8f33575_79871023',
  'file_dependency' => 
  array (
    'd916cf332ffe33ce36b9abba181f09bc3d606685' => 
    array (
      0 => 'C:\\Users\\Emir\\Desktop\\iKingsSGC\\templates\\forums.tpl',
      1 => 1456094554,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56d211b8f33575_79871023 ($_smarty_tpl) {
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, false);
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "content", array (
  0 => 'block_40756d211b8ed7273_58135995',
  1 => false,
  3 => 0,
  2 => 0,
));
}
/* {block 'content'}  file:forums.tpl */
function block_40756d211b8ed7273_58135995($_smarty_tpl, $_blockParentStack) {
?>


<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat1_0_saved_item = isset($_smarty_tpl->tpl_vars['cat1']) ? $_smarty_tpl->tpl_vars['cat1'] : false;
$_smarty_tpl->tpl_vars['cat1'] = new Smarty_Variable();
$__foreach_cat1_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat1_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat1']->value) {
$__foreach_cat1_0_saved_local_item = $_smarty_tpl->tpl_vars['cat1'];
?>

<div class="categories">
	<div class="main-category"><?php echo $_smarty_tpl->tpl_vars['cat1']->value['name'];?>
</div>
	
	<?php
$_from = $_smarty_tpl->tpl_vars['cat1']->value['children'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat2_1_saved_item = isset($_smarty_tpl->tpl_vars['cat2']) ? $_smarty_tpl->tpl_vars['cat2'] : false;
$_smarty_tpl->tpl_vars['cat2'] = new Smarty_Variable();
$__foreach_cat2_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat2_1_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat2']->value) {
$__foreach_cat2_1_saved_local_item = $_smarty_tpl->tpl_vars['cat2'];
?>
	<div class="sub-categories"> <!--unread-->
		<div class="category-icon"></div>
		<div class="category-title">
			<a href="categories/<?php echo $_smarty_tpl->tpl_vars['cat2']->value['id'];?>
/<?php echo urlencode(rawurlencode($_smarty_tpl->tpl_vars['cat2']->value['name']));?>
/"><?php echo $_smarty_tpl->tpl_vars['cat2']->value['name'];?>
</a><div class="category-new-omg">NEW</div>
			<br>
			<div class="category-comments">
				<i class="fa fa-comment"></i><span class="category-threads">32</span>
				<i class="fa fa-comments"></i><span class="category-threads">152</span>
			</div>
			<br>
			<?php if (!empty($_smarty_tpl->tpl_vars['cat2']->value['children'])) {?>
			<div class="category-subcategory-wrapper">
				<?php
$_from = $_smarty_tpl->tpl_vars['cat2']->value['children'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat3_2_saved_item = isset($_smarty_tpl->tpl_vars['cat3']) ? $_smarty_tpl->tpl_vars['cat3'] : false;
$_smarty_tpl->tpl_vars['cat3'] = new Smarty_Variable();
$__foreach_cat3_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat3_2_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat3']->value) {
$__foreach_cat3_2_saved_local_item = $_smarty_tpl->tpl_vars['cat3'];
?>
				<a href="categories/<?php echo $_smarty_tpl->tpl_vars['cat3']->value['id'];?>
/<?php echo urlencode(rawurlencode($_smarty_tpl->tpl_vars['cat3']->value['name']));?>
/" class="category-subcategory unread"><i class="fa fa-folder"></i><span><?php echo $_smarty_tpl->tpl_vars['cat3']->value['name'];?>
</span></a>
				<?php
$_smarty_tpl->tpl_vars['cat3'] = $__foreach_cat3_2_saved_local_item;
}
}
if ($__foreach_cat3_2_saved_item) {
$_smarty_tpl->tpl_vars['cat3'] = $__foreach_cat3_2_saved_item;
}
?>
			</div>
			<?php }?>
		</div>
	</div>
	<?php
$_smarty_tpl->tpl_vars['cat2'] = $__foreach_cat2_1_saved_local_item;
}
}
if ($__foreach_cat2_1_saved_item) {
$_smarty_tpl->tpl_vars['cat2'] = $__foreach_cat2_1_saved_item;
}
?>
</div>


<?php
$_smarty_tpl->tpl_vars['cat1'] = $__foreach_cat1_0_saved_local_item;
}
}
if ($__foreach_cat1_0_saved_item) {
$_smarty_tpl->tpl_vars['cat1'] = $__foreach_cat1_0_saved_item;
}
?>

<?php
}
/* {/block 'content'} */
}
