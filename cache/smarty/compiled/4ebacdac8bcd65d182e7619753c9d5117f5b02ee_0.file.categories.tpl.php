<?php
/* Smarty version 3.1.28, created on 2016-02-27 22:14:35
  from "C:\Users\Emir\Desktop\iKingsSGC\templates\categories.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56d211bb8ee0c8_52419254',
  'file_dependency' => 
  array (
    '4ebacdac8bcd65d182e7619753c9d5117f5b02ee' => 
    array (
      0 => 'C:\\Users\\Emir\\Desktop\\iKingsSGC\\templates\\categories.tpl',
      1 => 1456606659,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56d211bb8ee0c8_52419254 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\Users\\Emir\\Desktop\\iKingsSGC\\libraries\\smarty\\plugins\\modifier.date_format.php';
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, false);
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "content", array (
  0 => 'block_1718956d211bb81b917_48910000',
  1 => false,
  3 => 0,
  2 => 0,
));
}
/* {block 'content'}  file:categories.tpl */
function block_1718956d211bb81b917_48910000($_smarty_tpl, $_blockParentStack) {
?>



<div class="categories-full">
	<div class="main-category">
		<?php echo $_smarty_tpl->tpl_vars['category']->value['name'];?>

		<?php if ($_smarty_tpl->tpl_vars['show_create_btn']->value) {?>
		<a class="create-thread-button" href="createthread/<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
/<?php echo urlencode(rawurlencode($_smarty_tpl->tpl_vars['category']->value['name']));?>
/">Create thread</a><br>
		<?php }?>
	</div>
	<?php if (!empty($_smarty_tpl->tpl_vars['category']->value['children'])) {?>
	<div class="sub-categories-parent">
		<?php
$_from = $_smarty_tpl->tpl_vars['category']->value['children'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_child_0_saved_item = isset($_smarty_tpl->tpl_vars['child']) ? $_smarty_tpl->tpl_vars['child'] : false;
$_smarty_tpl->tpl_vars['child'] = new Smarty_Variable();
$__foreach_child_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_child_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['child']->value) {
$__foreach_child_0_saved_local_item = $_smarty_tpl->tpl_vars['child'];
?>
		<div class="sub-categories-full"> <!--unread-->
			<div class="category-icon"></div>
			<div class="category-title">
				<a href="categories/<?php echo $_smarty_tpl->tpl_vars['child']->value['id'];?>
/<?php echo urlencode(rawurlencode($_smarty_tpl->tpl_vars['child']->value['name']));?>
/"><?php echo $_smarty_tpl->tpl_vars['child']->value['name'];?>
</a><div class="category-new-omg">NEW</div>
				<br>
				<div class="category-comments">
					<i class="fa fa-comment"></i><span class="category-threads">32</span>
					<i class="fa fa-comments"></i><span class="category-threads">152</span>
				</div>
				<br>
			</div>
		</div>
		<?php
$_smarty_tpl->tpl_vars['child'] = $__foreach_child_0_saved_local_item;
}
}
if ($__foreach_child_0_saved_item) {
$_smarty_tpl->tpl_vars['child'] = $__foreach_child_0_saved_item;
}
?>
	</div>
	<?php }?>
</div>
<?php if ($_smarty_tpl->tpl_vars['category']->value['level'] != 0) {?>
	<?php
$_from = $_smarty_tpl->tpl_vars['threads']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_thread_1_saved_item = isset($_smarty_tpl->tpl_vars['thread']) ? $_smarty_tpl->tpl_vars['thread'] : false;
$_smarty_tpl->tpl_vars['thread'] = new Smarty_Variable();
$__foreach_thread_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_thread_1_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['thread']->value) {
$__foreach_thread_1_saved_local_item = $_smarty_tpl->tpl_vars['thread'];
?>
	<div class="threads">
		<div class="threads-table">
			<div class="threads-table-authoricon"></div>
			<div class="threads-table-name">
				<?php if ($_smarty_tpl->tpl_vars['thread']->value['is_official'] == 1) {?><div class="label label-official">OFFICIAL</div><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['thread']->value['is_sticky'] == 1) {?><div class="label label-sticky">STICKY</div><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['thread']->value['is_locked'] == 1) {?><div class="label label-locked">LOCKED</div><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['thread']->value['deleted'] == 1) {?><div class="label label-locked">DELETED</div><?php }?>
				<a href="threads/<?php echo $_smarty_tpl->tpl_vars['thread']->value['id'];?>
/<?php echo urlencode(rawurlencode($_smarty_tpl->tpl_vars['thread']->value['name']));?>
"><?php echo htmlentities($_smarty_tpl->tpl_vars['thread']->value['name']);?>
</a><br>
				<div class="threads-table-author"><a href="users/<?php echo $_smarty_tpl->tpl_vars['thread']->value['author_id'];?>
/<?php echo urlencode(rawurlencode($_smarty_tpl->tpl_vars['thread']->value['author']));?>
"><?php echo htmlentities($_smarty_tpl->tpl_vars['thread']->value['author']);?>
</a>, <a href="#"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['thread']->value['create_date'],'M d, Y');?>
</a></div>
			</div>
			<div class="threads-table-ratings-parent">
				<div class="threads-table-ratings">
					<span class="rating-icon rating-icon-like">x8</span>
					<span class="rating-icon rating-icon-dislike">x258</span>
				</div>
			</div>
			<div class="threads-table-replies">
				<div class="threads-table-replies-title">Replies:</div><div class="threads-table-replies-value">200</div><br>
				<div class="threads-table-views-title">Views:</div><div class="threads-table-views-value">1239</div>
			</div>
			<div class="threads-table-authorinfo">
				<div class="threads-table-lastpost"><a href="#">isteinvids</a></div><br>
				<div class="threads-table-createdate"><a href="#">Today at 10:50pm</a></div>
			</div>
		</div>
	</div>
	<?php
$_smarty_tpl->tpl_vars['thread'] = $__foreach_thread_1_saved_local_item;
}
}
if ($__foreach_thread_1_saved_item) {
$_smarty_tpl->tpl_vars['thread'] = $__foreach_thread_1_saved_item;
}
}?>

<?php
}
/* {/block 'content'} */
}
