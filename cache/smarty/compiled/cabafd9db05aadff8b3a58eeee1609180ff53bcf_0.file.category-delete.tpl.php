<?php
/* Smarty version 3.1.28, created on 2016-01-12 07:33:45
  from "C:\Users\Emir\Desktop\iKingsSGC\templates\admin\category-delete.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56949e49ed7e75_12069907',
  'file_dependency' => 
  array (
    'cabafd9db05aadff8b3a58eeee1609180ff53bcf' => 
    array (
      0 => 'C:\\Users\\Emir\\Desktop\\iKingsSGC\\templates\\admin\\category-delete.tpl',
      1 => 1452580424,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56949e49ed7e75_12069907 ($_smarty_tpl) {
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, false);
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "content", array (
  0 => 'block_809456949e49e2fd17_20920468',
  1 => false,
  3 => 0,
  2 => 0,
));
}
/* {block 'content'}  file:admin/category-delete.tpl */
function block_809456949e49e2fd17_20920468($_smarty_tpl, $_blockParentStack) {
?>

lalalala category <?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>

<br>
<form method="post">
	Where to move existing threads in this category? <br>
	<select name="threads">
		<option value="" selected></option>
		<option value="delete">DELETE ALL THREADS IN THIS CATEGORY</option>
		<option value="" disabled></option>
		<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat_0_saved_item = isset($_smarty_tpl->tpl_vars['cat']) ? $_smarty_tpl->tpl_vars['cat'] : false;
$_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable();
$__foreach_cat_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->value) {
$__foreach_cat_0_saved_local_item = $_smarty_tpl->tpl_vars['cat'];
?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['cat']->value['id'];?>
" disabled><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</option>
				<?php
$_from = $_smarty_tpl->tpl_vars['cat']->value['children'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat2_1_saved_item = isset($_smarty_tpl->tpl_vars['cat2']) ? $_smarty_tpl->tpl_vars['cat2'] : false;
$_smarty_tpl->tpl_vars['cat2'] = new Smarty_Variable();
$__foreach_cat2_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat2_1_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat2']->value) {
$__foreach_cat2_1_saved_local_item = $_smarty_tpl->tpl_vars['cat2'];
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['cat2']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['cat2']->value['id'] == $_smarty_tpl->tpl_vars['category']->value['id']) {?>disabled<?php }?>>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['cat2']->value['name'];?>
</option>
					<?php
$_from = $_smarty_tpl->tpl_vars['cat2']->value['children'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat3_2_saved_item = isset($_smarty_tpl->tpl_vars['cat3']) ? $_smarty_tpl->tpl_vars['cat3'] : false;
$_smarty_tpl->tpl_vars['cat3'] = new Smarty_Variable();
$__foreach_cat3_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat3_2_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat3']->value) {
$__foreach_cat3_2_saved_local_item = $_smarty_tpl->tpl_vars['cat3'];
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['cat3']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['cat3']->value['id'] == $_smarty_tpl->tpl_vars['category']->value['id']) {?>disabled<?php }?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['cat3']->value['name'];?>
</option>
					<?php
$_smarty_tpl->tpl_vars['cat3'] = $__foreach_cat3_2_saved_local_item;
}
}
if ($__foreach_cat3_2_saved_item) {
$_smarty_tpl->tpl_vars['cat3'] = $__foreach_cat3_2_saved_item;
}
?>
				<?php
$_smarty_tpl->tpl_vars['cat2'] = $__foreach_cat2_1_saved_local_item;
}
}
if ($__foreach_cat2_1_saved_item) {
$_smarty_tpl->tpl_vars['cat2'] = $__foreach_cat2_1_saved_item;
}
?>
		<?php
$_smarty_tpl->tpl_vars['cat'] = $__foreach_cat_0_saved_local_item;
}
}
if ($__foreach_cat_0_saved_item) {
$_smarty_tpl->tpl_vars['cat'] = $__foreach_cat_0_saved_item;
}
?>
	</select>
	<br>
	<?php if (!empty($_smarty_tpl->tpl_vars['category']->value['children'])) {?>
	Where to move existing categories in this category? <br>
	<select name="threads">
		<option value="" selected></option>
		<option value="delete">DELETE ALL CATEGORIES IN THIS CATEGORY</option>
		<option value="" disabled></option>
		<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat_3_saved_item = isset($_smarty_tpl->tpl_vars['cat']) ? $_smarty_tpl->tpl_vars['cat'] : false;
$_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable();
$__foreach_cat_3_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat_3_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->value) {
$__foreach_cat_3_saved_local_item = $_smarty_tpl->tpl_vars['cat'];
?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['cat']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['cat']->value['id'] == $_smarty_tpl->tpl_vars['category']->value['id']) {?>disabled<?php }?>><?php echo $_smarty_tpl->tpl_vars['cat']->value['name'];?>
</option>
				<?php
$_from = $_smarty_tpl->tpl_vars['cat']->value['children'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat2_4_saved_item = isset($_smarty_tpl->tpl_vars['cat2']) ? $_smarty_tpl->tpl_vars['cat2'] : false;
$_smarty_tpl->tpl_vars['cat2'] = new Smarty_Variable();
$__foreach_cat2_4_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat2_4_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat2']->value) {
$__foreach_cat2_4_saved_local_item = $_smarty_tpl->tpl_vars['cat2'];
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['cat2']->value['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['cat2']->value['id'] == $_smarty_tpl->tpl_vars['category']->value['id']) {?>disabled<?php }?>>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['cat2']->value['name'];?>
</option>
					<?php
$_from = $_smarty_tpl->tpl_vars['cat2']->value['children'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat3_5_saved_item = isset($_smarty_tpl->tpl_vars['cat3']) ? $_smarty_tpl->tpl_vars['cat3'] : false;
$_smarty_tpl->tpl_vars['cat3'] = new Smarty_Variable();
$__foreach_cat3_5_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_cat3_5_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat3']->value) {
$__foreach_cat3_5_saved_local_item = $_smarty_tpl->tpl_vars['cat3'];
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['cat3']->value['id'];?>
" disabled>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['cat3']->value['name'];?>
</option>
					<?php
$_smarty_tpl->tpl_vars['cat3'] = $__foreach_cat3_5_saved_local_item;
}
}
if ($__foreach_cat3_5_saved_item) {
$_smarty_tpl->tpl_vars['cat3'] = $__foreach_cat3_5_saved_item;
}
?>
				<?php
$_smarty_tpl->tpl_vars['cat2'] = $__foreach_cat2_4_saved_local_item;
}
}
if ($__foreach_cat2_4_saved_item) {
$_smarty_tpl->tpl_vars['cat2'] = $__foreach_cat2_4_saved_item;
}
?>
		<?php
$_smarty_tpl->tpl_vars['cat'] = $__foreach_cat_3_saved_local_item;
}
}
if ($__foreach_cat_3_saved_item) {
$_smarty_tpl->tpl_vars['cat'] = $__foreach_cat_3_saved_item;
}
?>
	</select>
	<br>
	<?php }?>
	<button>OK</button>
</form>
<?php
}
/* {/block 'content'} */
}
