<?php
/* Smarty version 3.1.28, created on 2016-02-27 22:51:17
  from "C:\Users\Emir\Desktop\iKingsSGC\templates\main.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56d21a553b9cf3_82303606',
  'file_dependency' => 
  array (
    '477eef9b985460ff3eb1038b85fc55616d084a9c' => 
    array (
      0 => 'C:\\Users\\Emir\\Desktop\\iKingsSGC\\templates\\main.tpl',
      1 => 1456093836,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56d21a553b9cf3_82303606 ($_smarty_tpl) {
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Forum</title>
		<base href="<?php echo $_smarty_tpl->tpl_vars['basehref']->value;?>
">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<link href='//fonts.googleapis.com/css?family=Open+Sans:400,700|Lato:400,700' rel='stylesheet' type='text/css'>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel='stylesheet' type='text/css'>
		
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/wbbtheme.css">
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		<link href="assets/css/style.css" rel='stylesheet' type='text/css'>

		<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/bootstrap.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/jquery.wysibb.min.js"><?php echo '</script'; ?>
>
	</head>
	<body>
		<div class="pageWidth">
			<div class="top_bar <?php if (isset($_SESSION['user'])) {?>loggedin<?php }?>">
				<?php if (isset($_SESSION['user'])) {?>
				<div class="btnlogout"><a href="users/<?php echo $_SESSION['user']['id'];?>
/<?php echo urlencode(rawurlencode($_SESSION['user']['username']));?>
/"><?php echo $_SESSION['user']['username'];?>
</a> - <a href="logout">Logout</a></div>
				<?php } else { ?>
				<div class="btnlogin"><a href="login?from=<?php echo urlencode($_SERVER['PATH_INFO']);?>
">Login</a> or <a href="register?from=<?php echo urlencode($_SERVER['PATH_INFO']);?>
">Register</a></div>
				<?php }?>
			</div>
			<div class="logo"></div>
			<div class="clear"></div>
			<div class="navbar">
				<div class="menu">
					<a class="menuitem" href="home">Home</a>
					<a class="menuitem selected" href="forums">Forums</a>
					<a class="menuitem" href="resources">Resources</a>
					<a class="menuitem">Servers</a>
					<a class="menuitem">Mod Division</a>
					<a class="menuitem">Stargate Channel</a>
					<a class="menuitem">Donations</a>
					<a class="menuitem">12 Days 2015</a>
				</div>
			</div>
			<?php 
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "content", array (
  0 => 'block_656956d21a553b10a8_69344078',
  1 => false,
  3 => 0,
  2 => 0,
));
?>

		</div>
		
		<?php echo '<script'; ?>
>
			var settings = {
				buttons:"bold,italic,underline,strike,sup,sub,|,img,video,link,|,bullist,numlist,|,fontcolor,fontsize,fontfamily,|,justifyleft,justifycenter,justifyright,|,quote,code,removeFormat",
				allButtons: {
					bullist : {
						title: CURLANG.bullist,
						buttonHTML: '<span class="fonticon ve-tlb-list1">\uE009</span>',
						excmd: 'insertUnorderedList',
						transform : {
							'<ul>{SELTEXT}</ul>':"[ulist]{SELTEXT}[/list]",
							'<li>{SELTEXT}</li>':"[li]{SELTEXT}[/li]"
						}
					},
					numlist : {
						title: CURLANG.numlist,
						buttonHTML: '<span class="fonticon ve-tlb-numlist1">\uE00a</span>',
						excmd: 'insertOrderedList',
						transform : {
							'<ol>{SELTEXT}</ol>':"[list]{SELTEXT}[/list]",
							'<li>{SELTEXT}</li>':"[li]{SELTEXT}[/li]"
						}
					}
				}
			};
			$(".wysibb").wysibb(settings);
		<?php echo '</script'; ?>
>
		
	</body>
</html><?php }
/* {block 'content'}  file:main.tpl */
function block_656956d21a553b10a8_69344078($_smarty_tpl, $_blockParentStack) {
?>
Reading from main.tpl<?php
}
/* {/block 'content'} */
}
