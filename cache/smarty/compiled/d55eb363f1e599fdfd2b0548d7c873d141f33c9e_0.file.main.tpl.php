<?php
/* Smarty version 3.1.28, created on 2016-03-18 07:43:18
  from "E:\PHP\encryptedimagehosting\templates\main.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56eba3866a3659_32551515',
  'file_dependency' => 
  array (
    'd55eb363f1e599fdfd2b0548d7c873d141f33c9e' => 
    array (
      0 => 'E:\\PHP\\encryptedimagehosting\\templates\\main.tpl',
      1 => 1458281046,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56eba3866a3659_32551515 ($_smarty_tpl) {
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Forum</title>
		<base href="<?php echo $_smarty_tpl->tpl_vars['basehref']->value;?>
">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
		
		<link href="assets/css/style.css" rel='stylesheet' type='text/css'>
		
		<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"><?php echo '</script'; ?>
>
		
		<?php echo '<script'; ?>
 src="assets/js/crypto/core-min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/crypto/md5-min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/crypto/evpkdf-min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/crypto/enc-base64-min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/crypto/cipher-core-min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="assets/js/crypto/aes-min.js"><?php echo '</script'; ?>
>
	</head>
	<body>
		<?php 
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "body", array (
  0 => 'block_574456eba386673401_31908502',
  1 => false,
  3 => 0,
  2 => 0,
));
?>

		<?php echo '<script'; ?>
 src="assets/js/app.js"><?php echo '</script'; ?>
>
	</body>
</html>
<?php }
/* {block 'content'}  file:main.tpl */
function block_2404156eba38669bc68_12559341($_smarty_tpl, $_blockParentStack) {
?>
Reading from main.tpl<?php
}
/* {/block 'content'} */
/* {block 'body'}  file:main.tpl */
function block_574456eba386673401_31908502($_smarty_tpl, $_blockParentStack) {
?>

		<div class="header">
			<a href="home">
				<div class="title"></div>
			</a>
			<div class="links">
				<?php if (isset($_SESSION['user'])) {?>
				<a href="#"><?php echo $_SESSION['user']['username'];?>
</a>
				<a href="logout?from=<?php echo urlencode($_SERVER['PATH_INFO']);?>
">LOGOUT</a>
				<?php } else { ?>
				<a href="login?from=<?php echo urlencode($_SERVER['PATH_INFO']);?>
">LOGIN</a>
				<a href="register?from=<?php echo urlencode($_SERVER['PATH_INFO']);?>
">REGISTER</a>
				<?php }?>
			</div>
		</div>
		<div class="content">
			<?php 
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "content", array (
  0 => 'block_2404156eba38669bc68_12559341',
  1 => false,
  3 => 0,
  2 => 0,
), $_blockParentStack);
?>

		</div>
		<div class="footer">
			<div class="links">
				<a href="#">ABOUT</a>
				<a href="#">PRIVACY POLICY</a>
			</div>
		</div>
		<?php
}
/* {/block 'body'} */
}
