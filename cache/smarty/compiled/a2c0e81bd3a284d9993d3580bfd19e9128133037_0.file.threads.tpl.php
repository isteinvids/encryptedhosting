<?php
/* Smarty version 3.1.28, created on 2016-02-27 22:51:17
  from "C:\Users\Emir\Desktop\iKingsSGC\templates\threads.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_56d21a5533d7f0_74485434',
  'file_dependency' => 
  array (
    'a2c0e81bd3a284d9993d3580bfd19e9128133037' => 
    array (
      0 => 'C:\\Users\\Emir\\Desktop\\iKingsSGC\\templates\\threads.tpl',
      1 => 1453236710,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56d21a5533d7f0_74485434 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\Users\\Emir\\Desktop\\iKingsSGC\\libraries\\smarty\\plugins\\modifier.date_format.php';
$_smarty_tpl->ext->_inheritance->init($_smarty_tpl, false);
$_smarty_tpl->ext->_inheritance->processBlock($_smarty_tpl, 0, "content", array (
  0 => 'block_1393956d21a552b71e3_60993006',
  1 => false,
  3 => 0,
  2 => 0,
));
}
/* {block 'content'}  file:threads.tpl */
function block_1393956d21a552b71e3_60993006($_smarty_tpl, $_blockParentStack) {
?>


<div class="thread-title">
	<div class="thread-title-title"><?php if ($_smarty_tpl->tpl_vars['threads']->value[0]['is_locked'] == 1) {?><b style="color:red;">LOCKED</b> <?php }
echo htmlentities($_smarty_tpl->tpl_vars['threads']->value[0]['name']);?>
</div>
	<div class="thread-title-author">Discussion in <b><a href="#">General Topics</a></b> started by <b><a href="#">isteinvids</a></b>, <b><a href="#">Dec 21, 2018</a></b></div>
</div>
<?php
$_from = $_smarty_tpl->tpl_vars['threads']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_thread_0_saved_item = isset($_smarty_tpl->tpl_vars['thread']) ? $_smarty_tpl->tpl_vars['thread'] : false;
$_smarty_tpl->tpl_vars['thread'] = new Smarty_Variable();
$__foreach_thread_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_thread_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['thread']->value) {
$__foreach_thread_0_saved_local_item = $_smarty_tpl->tpl_vars['thread'];
?>
<div class="thread">
	<div class="thread-author-wrapper">
		<img class="thread-author-icon" src="assets/images/profile.png">
		<img class="thread-author-rank" src="assets/images/admin.png">
		<div class="thread-author-name"><a href="users/<?php echo $_smarty_tpl->tpl_vars['thread']->value['author_id'];?>
/<?php echo urlencode($_smarty_tpl->tpl_vars['thread']->value['author']);?>
"><?php echo htmlentities($_smarty_tpl->tpl_vars['thread']->value['author']);?>
</a></div>
		<div class="thread-author-title"><?php echo htmlentities($_smarty_tpl->tpl_vars['thread']->value['author_title']);?>
</div>
	</div>
	<div class="thread-content-wrapper">
		<div class="thread-content"><?php echo $_smarty_tpl->tpl_vars['thread']->value['content'];?>
</div>
		<div class="thread-ratings">
			<div class="thread-ratings-icons">
				<a class="rating-undo">Undo Rating</a>
				<span class="rating-icon rating-icon-like" action="like"></span>
				<span class="rating-icon rating-icon-dislike" action="dislike"></span>
				<span class="rating-icon rating-icon-agree" action="agree"></span>
				<span class="rating-icon rating-icon-disagree" action="disagree"></span>
			</div>
			<div class="thread-ratings-words" threadid="<?php echo $_smarty_tpl->tpl_vars['thread']->value['id'];?>
" <?php if (!isset($_smarty_tpl->tpl_vars['thread']->value['ratings'])) {?>style="display:none"<?php }?>>
				<?php
$_from = $_smarty_tpl->tpl_vars['thread']->value['ratings'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_rate_1_saved_item = isset($_smarty_tpl->tpl_vars['rate']) ? $_smarty_tpl->tpl_vars['rate'] : false;
$__foreach_rate_1_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['rate'] = new Smarty_Variable();
$__foreach_rate_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_rate_1_total) {
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['rate']->value) {
$__foreach_rate_1_saved_local_item = $_smarty_tpl->tpl_vars['rate'];
?>
				<span class="rating-icon rating-icon-<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
 rating-icon-small"> <?php echo ucwords($_smarty_tpl->tpl_vars['key']->value);?>
 * <b><?php echo $_smarty_tpl->tpl_vars['rate']->value;?>
</b></span>&nbsp;&nbsp;
				<?php
$_smarty_tpl->tpl_vars['rate'] = $__foreach_rate_1_saved_local_item;
}
}
if ($__foreach_rate_1_saved_item) {
$_smarty_tpl->tpl_vars['rate'] = $__foreach_rate_1_saved_item;
}
if ($__foreach_rate_1_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_rate_1_saved_key;
}
?>
<!--
				<span class="rating-icon rating-icon-dislike rating-icon-small"> Dislike * <b>45</b></span>&nbsp;&nbsp;
				<span class="rating-icon rating-icon-agree rating-icon-small rating-yours"> Agree * <b>45</b></span>&nbsp;&nbsp;
				<span class="rating-icon rating-icon-dislike rating-icon-small"> Dislike * <b>45</b></span>&nbsp;&nbsp;
				<span class="rating-icon rating-icon-agree rating-icon-small"> Agree * <b>45</b></span>&nbsp;&nbsp;
				<span class="rating-icon rating-icon-dislike rating-icon-small"> Dislike * <b>45</b></span>&nbsp;&nbsp;
-->
			</div>
		</div>
		<div style="clear:both;"></div>
		<div class="thread-ratings-author">
			<a href="users/<?php echo $_smarty_tpl->tpl_vars['thread']->value['author_id'];?>
/<?php echo urlencode($_smarty_tpl->tpl_vars['thread']->value['author']);?>
"><?php echo $_smarty_tpl->tpl_vars['thread']->value['author'];?>
</a> at <a href="threads/<?php echo $_smarty_tpl->tpl_vars['threads']->value[0]['id'];?>
/<?php echo urlencode(rawurlencode($_smarty_tpl->tpl_vars['threads']->value[0]['name']));?>
/#<?php echo $_smarty_tpl->tpl_vars['thread']->value['id'];?>
"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['thread']->value['create_date'],'M d, Y');?>
</a>
		</div>
		<div style="clear:both;"></div>
		<div class="thread-controls">
			<a href="#" class="thread-controls-button"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit</a>
		</div>
	</div>
</div>
<?php
$_smarty_tpl->tpl_vars['thread'] = $__foreach_thread_0_saved_local_item;
}
}
if ($__foreach_thread_0_saved_item) {
$_smarty_tpl->tpl_vars['thread'] = $__foreach_thread_0_saved_item;
}
if ($_smarty_tpl->tpl_vars['cancomment']->value) {?>
<form method="post" id="threadform">
	<div id="editor_parent">
		<textarea id="editor" name="content" class="wysibb"></textarea>
	</div>
	<button>OK LOL</button>
</form>
<?php echo '<script'; ?>
>
	$("#threadform").submit(function(){
		$("#editor").val($("#editor").bbcode());
	});
	$(".rating-icon").click(function(){
		var action = $(this).attr("action");
		var $parent = $(this).closest(".thread-ratings");
		var $words = $parent.children(".thread-ratings-words");
		$.post("ajax/dorate", { "threadid": $words.attr("threadid"), "action": action }, function(data) {
			if(data.trim() !== ""){
				$words.show();
				$words.html(data);
			} else {
				$words.hide();
			}
		});
	});
	$(".thread-ratings-words").each(function(){
		var $thread = $(this);
	});
//	$.post("ajax/rate", { "threadid": 12, "rate": "like" }, function(data) { console.log(data) })	
<?php echo '</script'; ?>
>
<?php }?>



<?php
}
/* {/block 'content'} */
}
