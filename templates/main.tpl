<!DOCTYPE html>
<html>
	<head>
		<title>Forum</title>
		<base href="{$basehref}">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
		
		<link href="assets/css/style.css" rel='stylesheet' type='text/css'>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		
		<script src="assets/js/crypto/core-min.js"></script>
		<script src="assets/js/crypto/md5-min.js"></script>
		<script src="assets/js/crypto/evpkdf-min.js"></script>
		<script src="assets/js/crypto/enc-base64-min.js"></script>
		<script src="assets/js/crypto/cipher-core-min.js"></script>
		<script src="assets/js/crypto/aes-min.js"></script>
	</head>
	<body>
		{block name="body"}
		<div class="header">
			<a href="home">
				<div class="title"></div>
			</a>
			<div class="links">
				{if isset($smarty.session['user'])}
				<a href="#">{$smarty.session['user']['username']}</a>
				<a href="logout?from={$smarty.server['PATH_INFO']|urlencode}">LOGOUT</a>
				{else}
				<a href="login?from={$smarty.server['PATH_INFO']|urlencode}">LOGIN</a>
				<a href="register?from={$smarty.server['PATH_INFO']|urlencode}">REGISTER</a>
				{/if}
			</div>
		</div>
		<div class="content">
			{block name="content"}Reading from main.tpl{/block}
		</div>
		<div class="footer">
			<div class="links">
				<a href="#">ABOUT</a>
				<a href="#">PRIVACY POLICY</a>
			</div>
		</div>
		{/block}
		<script src="assets/js/app.js"></script>
	</body>
</html>
