{literal}
<html>
	<head>
		<title>IM</title>
		<script src="assets/js/jsencrypt.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.4/angular-material.min.js"></script>
		<script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
		
		<script src="assets/js/crypto/core-min.js"></script>
		<script src="assets/js/crypto/md5-min.js"></script>
		<script src="assets/js/crypto/evpkdf-min.js"></script>
		<script src="assets/js/crypto/enc-base64-min.js"></script>
		<script src="assets/js/crypto/cipher-core-min.js"></script>
		<script src="assets/js/crypto/aes-min.js"></script>
	</head>
	<body>
		<style>
			a {
				color: blue;
				text-decoration: underline;
				cursor: pointer;
			}
		</style>
		<div ng-controller="mainController" ng-app="EncryptedMessaging">
			<div user="UX86Nmf1HP"></div>
			<div ng-repeat="(key, value) in messages[talkingTo]"><b ng-if="value.from">{{value.from}}: </b> {{value.msg}}</div>
			<form id="chatform" ng-submit="onChatInput(msg)">
				<input type="text" ng-model="msg">
				<button>ENTER</button>
			</form>
			<br>
			<div ng-repeat="(key, value) in keys"><a ng-click="startTalkWith(key, true)">{{key}}</a> | </div>
		</div>
		{/literal}<script>var userInfo = {$info};</script>{literal}
		<script src="assets/js/im.js"></script>
	</body>
</html>
{/literal}