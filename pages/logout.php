<?php

	$_users->setCookiesUsed();

	if(isset($_SESSION['user'])){
		unset($_SESSION['user']);
	}

	$redirect("home");