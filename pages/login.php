<?php

	if(isset($_SESSION['user'])){
		$redirect("home");
		exit;
	}

	$errormsg = "";

	if($_POST){
		$errormsg = $_users->login($_POST['username'], $_POST['password']);
		if($errormsg === true){
			if(isset($_POST['rememberme']) && $_POST['rememberme'] == 'on'){
				$_users->addNewCookie($_SESSION['user']['id']);
			}
			$redirect(isset($_GET['from']) ? substr($_GET['from'], 1) : "forums");
		}
	}

	$view->assign("errormsg", $errormsg);

	$display('login');
