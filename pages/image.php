<?php

	if(!isset($path[1])){
		error404();
	}

	$imageid = $path[1];

	$image = $_images->getImage($imageid);

	$comments = $_images->getCommentsForMD5($image['rand'], false);

	$view->assign("image", $image);
	$view->assign("comments", $comments);

	$display('image');
