<?php

	if(isset($_SESSION['user'])){
		$redirect("home");
	}

	$errormsg = "";

	if($_POST){
		$errormsg = $_users->register($_POST);
		if($errormsg === true){
			$redirect(isset($_GET['from']) ? substr($_GET['from'], 1) : "login");
		}
	}

	$view->assign("errormsg", $errormsg);

	$display('register');
