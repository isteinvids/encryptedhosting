<?php

	$path = substr($_SERVER['PATH_INFO'], 1);
	
	$mime_types = array(
		'.css' => 'text/css',
		'.eot'=>'application/vnd.ms-fontobject',
		'.htm' => 'text/html',
		'.html' => 'text/html',
		'.jpeg' => 'image/jpeg',
		'.jpg' => 'image/jpeg',
		'.js' => 'application/x-javascript',
		'.mp3' => 'audio/mpeg3',
		'.png' => 'image/png',
		'.shtml' => 'text/x-server-parsed-html',
		'.svg'=>'image/svg+xml',
		'.swf' => 'application/x-shockwave-flash',
		'.tif' => 'image/x-tiff',
		'.tiff' => 'image/tiff',
		'.tiff' => 'image/x-tiff',
		'.ttf'=>'application/font-sfnt',
		'.txt' => 'text/plain',
		'.wav' => 'audio/wav',
		'.woff'=>'application/font-woff',
		'.xml' => 'application/xml',
		'.x-png' => 'image/png',
		'.zip' => 'application/zip'
	);
	
	$extension = substr($path, strrpos($path, "."), strlen($path));
	
	if(!file_exists($path) || !isset($mime_types[$extension])){
		error404();
	}
	
	
	$fileModTime = filemtime($path);

	session_cache_limiter('private_no_cache');

	if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $fileModTime)) { 
		header('Last-Modified: '.gmdate('D, d M Y H:i:s', $fileModTime).' GMT', true, 304); 
	} else {
		header('Pragma: public');
		header('Cache-Control: max-age=86400');
		header('Last-Modified: '.gmdate('D, d M Y H:i:s', $fileModTime).' GMT', true, 200);
		header("Content-Type: {$mime_types[$extension]}");
		
		//	header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + (86400 * 14)));
		//    header('Content-length: '.filesize($path));
		//    header('Content-transfer-encoding: binary');
		
		print(file_get_contents($path));
	}
	
	exit;