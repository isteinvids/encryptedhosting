<?php

	$userid = isset($_SESSION['user']) ? $_SESSION['user']['id'] : '';

	if(isset($path[1])){
		switch($path[1]){
			case 'upload':
				//if last upload was more than a minute ago, or something like that
				
				$encrypted = isset($_POST['encrypted']) ? $_POST['encrypted'] : false;
				$md5 = isset($_POST['md5']) ? $_POST['md5'] : false;
				$rand = $_images->saveImage($encrypted, $md5, $userid);
				
				print($rand);
				exit;
			break;
			case 'getimage':
				
				$rand = isset($_GET['rand']) ? $_GET['rand'] : false;
				$image = $_images->getImage($rand);
				
				$fileModTime = strtotime($image['create_date']);
	
//				session_cache_limiter('public');
				
				if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $fileModTime)) { 
					header('Last-Modified: '.gmdate('D, d M Y H:i:s', $fileModTime).' GMT', true, 304); 
				} else {
					header('Pragma: public');
					header('Cache-Control: max-age=86400');
					header('Last-Modified: '.gmdate('D, d M Y H:i:s', $fileModTime).' GMT', true, 200);
					header("Content-Type: text/plain");

					print($image['encrypted']);
				}
				exit;
			break;
			case 'getcomments':
				
				$rand = isset($_POST['rand']) ? $_POST['rand'] : false;
				$lastid = isset($_POST['lastid']) ? $_POST['lastid'] : false;
				
				$comments = $_images->getCommentsForMD5($rand, false, $lastid);
				
				$view->assign("comments", $comments);
				
				$view->display("image_comment.tpl");
				exit;
			break;
			case 'postcomment':
				
				$text = isset($_POST['text']) ? $_POST['text'] : false;
				$rand = isset($_POST['rand']) ? $_POST['rand'] : false;
				$md5 = isset($_POST['md5']) ? $_POST['md5'] : false;
				
				if($userid !== ''){
					$_images->addComment($rand, $text, $md5, $userid);
					print("ok");
					exit;
				}
				
			break;
		}
	}

	print('not ok');
//	var_dump($path);
