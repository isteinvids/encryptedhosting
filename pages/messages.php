<?php

	if(!isset($_SESSION['user'])){
		$redirect("home");
	}

	$info = json_encode(array("username"=>$_SESSION['user']['username'], "rand"=>$_users->generateRand($_SESSION['user']['id'])));

	$view->assign("info", $info);

	$view->display("messages.tpl");
