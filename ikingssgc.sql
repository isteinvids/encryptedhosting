-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2016 at 07:50 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ikingssgc`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(3) NOT NULL,
  `name` varchar(64) NOT NULL,
  `create_date` datetime NOT NULL,
  `staff_only_can_post` int(2) NOT NULL DEFAULT '0',
  `hidden_for_nonstaff` int(2) NOT NULL DEFAULT '0',
  `manual_sort` int(4) NOT NULL,
  `level` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent`, `name`, `create_date`, `staff_only_can_post`, `hidden_for_nonstaff`, `manual_sort`, `level`) VALUES
(1, 0, 'iKingsSGC Topics', '2016-01-10 16:47:39', 0, 0, 0, 0),
(2, 1, 'iKingsSGCâ„¢ Topics', '2016-01-01 00:00:00', 0, 0, 6, 1),
(3, 1, 'Donations', '0000-00-00 00:00:00', 1, 1, 5, 1),
(4, 1, 'Member Discussion / General Topics', '2016-01-12 00:00:00', 0, 0, 1, 1),
(6, 4, 'Introduce Yourself', '2016-01-12 00:00:00', 1, 0, 4, 2),
(7, 4, 'Normal Topics', '2016-01-12 00:00:00', 0, 0, 3, 2),
(8, 4, 'New category name', '0000-00-00 00:00:00', 0, 0, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE IF NOT EXISTS `threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(4) NOT NULL,
  `name` varchar(128) NOT NULL,
  `content` text NOT NULL,
  `category` int(4) NOT NULL,
  `parent` int(4) NOT NULL DEFAULT '-1',
  `is_official` int(2) NOT NULL DEFAULT '0',
  `is_sticky` int(2) NOT NULL DEFAULT '0',
  `is_locked` int(2) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`id`, `author`, `name`, `content`, `category`, `parent`, `is_official`, `is_sticky`, `is_locked`, `create_date`) VALUES
(1, 1, 'LALALALALALALALA', '[center][b]sdlfmsdmfklsdmf[/b]\nertrltlfg\n[b]dfglkdfjgks[/b]\nslkdmflk\n[b]rwelkwmerl\n[/b]lknasdlk[/center][list][*]kermtlkermt\n[/*][*]lkmsflkmsdlfk[/*][*]sdfsdfm\n\n[center]sdsfsdfsssssssssssssssdfsdfsdfsdf\n[/center]\n[/*][/list][list=1][*]sdfsdfsdfsdfsdf\n[/*][*]dgfdgdfg[/*][*]gfhgfh[/*][*]tryertert[/*][/list]', 8, -1, 0, 0, 1, '2016-01-12 19:48:19'),
(2, 1, 'ey this is thread name ay', 'lkmflksdmflkmsdlkfmslkdmflkmsdlkfm\r\newrlnwerkwnekrjn\r\nasdlkamsdlkmasd\r\nklsdmflksdmfklmdsf\r\nnew forum!!!!!!\r\n[b]lalalalalalalalalala[/b]', 8, -1, 0, 0, 0, '2016-01-12 20:54:37'),
(3, 2, '', 'ksnf[b]ksdmf[u]klmsd[/u][i][u]lkf[/u]ms[/i][/b][i]dlkf[/i]m', 0, 1, 0, 0, 0, '2016-01-12 22:11:33'),
(4, 1, '', 'asdasdasdasd', 0, 2, 0, 0, 0, '2016-01-13 06:53:01'),
(5, 1, '', '<b>asdasdasd</b>', 0, 1, 0, 0, 0, '2016-01-13 07:37:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(128) NOT NULL,
  `is_staff` int(2) NOT NULL,
  `is_banned` int(2) NOT NULL,
  `is_admin` int(2) NOT NULL,
  `is_hidden` int(2) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `is_staff`, `is_banned`, `is_admin`, `is_hidden`, `create_date`) VALUES
(1, 'isteinvids', '$2y$12$DVAWbTbzK0w9IT.vRJVDt.lVW/G0HFHNayoeb.RsbVIDF40oWffOu', 'emirrhouni@gmail.com', 1, 0, 1, 0, '0000-00-00 00:00:00'),
(2, 'theiking', '$2y$12$GInp/K9ZuR0sLsYgcVo7e.MumjujuP2xdDP9y6BJzVjFWenOal2yK', 'theiking@gmail.com', 0, 0, 0, 0, '2016-01-11 21:03:57');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
