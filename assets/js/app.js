var lastId = 0;
var imagemd5 = "";
var rand = "";
var key = location.hash.substr(1);

if(document.getElementById("file")){
	rand = document.getElementById("file").getAttribute("data");
	// proof that we decrypted the file and have the right key...
	// we can keep an md5 of the key instead, but then it'd be easier for us to get the key by cracking the md5
	$.get("ajax/getimage", {rand:rand}, function(data){
		if(data){
			if(key.length == 64){
				var errored = true;
				try {
					var imagedat = decrypt(data, key);
					if(imagedat){
						$("#commentform").show();
						imagemd5 = md5(imagedat);
						document.getElementById("decryptedimage").src = imagedat;
						errored = false;
					}
					decryptComments(key);
				} catch(exc){
				}
				if(errored){
					document.getElementById("error").innerHTML = "invalid key :(";
				}
			}
		}
	});
}
$("#commentform").submit(function(){
	var key = location.hash.substr(1);
	var val = $("#commentform textarea").val();
	var lft = (140 - val.length);
	if(lft >= 0){
		var text = encrypt(val, key);
		$("#commentform textarea").val("");
		$.post("ajax/postcomment", {rand:rand,md5:imagemd5,text:text}, function(){
			updateCommentBoard();
		});
	}
	return false;
});
$("#commentform textarea").on("input", function(){
	var val = $(this).val();
	var lft = (140 - val.length);
	$("#commentform .charactersleft").html(lft+" characters left");
	if(lft < 0){
		$("#commentform .charactersleft").css("color", "red");
	} else {
		$("#commentform .charactersleft").css("color", "");
	}
});
function updateCommentBoard(){
	var biggest = false;
	$(".commentboard").children().each(function(){
		if(biggest < $(this).attr("commentid") || biggest === false){
			biggest = parseInt($(this).attr("commentid"));
		}
	});
	$.post("ajax/getcomments", {rand:rand,lastid:biggest}, function(data){
		$(".commentboard").append(data);
		decryptComments();
	});
}
function decryptComments(){
	$(".comment .message").each(function(){
		$(this).html(decrypt($(this).attr("msg"), key));
	});
}
function genkey() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for( var i = 0; i < 64; i++ ){
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}
function encrypt(plaintext, key){
	return CryptoJS.AES.encrypt(plaintext, key).toString();
}
function decrypt(encrypted, key){
	return CryptoJS.AES.decrypt(encrypted, key).toString(CryptoJS.enc.Utf8);
}
function md5(str){
	return CryptoJS.MD5(str).toString();
}

(function(){
	var uploader = document.getElementById("uploader");
	var tosend = {};
	var key = genkey();

	
	if(uploader){
		uploader.onchange = function(){
			var reader = new FileReader();

			reader.onload = function (e) {
				var data = e.target.result;
				document.getElementById("preview").src = data;

				var md5ed = md5(data);
				var encrypted = encrypt(data, key);

				tosend = {encrypted:encrypted,md5:md5ed};

			};

			var dataUrl = reader.readAsDataURL(this.files[0]);
		};

		document.getElementById("uploadform").onsubmit = function(e){
			$.post("ajax/upload", tosend, function(data){
				location.href = "image/"+data+"#"+key;
			});
			e.preventDefault();
			return false;
		}
	}
})();