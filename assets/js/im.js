	function encrypt(plaintext, key){
		return CryptoJS.AES.encrypt(plaintext, key).toString();
	}
	function decrypt(encrypted, key){
		return CryptoJS.AES.decrypt(encrypted, key).toString(CryptoJS.enc.Utf8);
	}
	function genrand(len){
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for( var i=0; i < len; i++ )
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}

	var aesKeys = {};
//	var socket = io.connect("http://localhost:8081");
	var socket = io.connect("http://yofreke.com:4445");
	var app = angular.module("EncryptedMessaging", []);

	app.controller("mainController", function ($scope, $http) {
		var mycrypt = new JSEncrypt({default_key_size: 1024});
		var othercrypt = new JSEncrypt({default_key_size: 1024});
		function generateKeys() {
			var dt = new Date();
			var time = -(dt.getTime());
			mycrypt.getKey();
			var dt = new Date();
			time += (dt.getTime());
		}
		function onReceiveOtherKey(publickey){
			othercrypt.setPublicKey(publickey);
		}
		socket.on("connect", function(){
			socket.emit("handshake", {userId:userInfo['rand'],publicKey:mycrypt.getPublicKey()});
			console.log("sending handshake");
		});
		socket.on("sendonlinepeople", function(data){
			$scope.keys = data.keys;
			$scope.$apply();
		});
		socket.on("sendmsg", function(data){
			if(aesKeys[data.username].fin){
				if(!$scope.messages[data.username]){
					$scope.messages[data.username] = [];
				}
				if(!$scope.talkingTo){
					$scope.talkingTo = data.username;
				}
				$scope.messages[data.username].push({from:data.username,msg:decrypt(data.msg, aesKeys[data.username].fin)});
				console.log(data);
			} else {
				$scope.startTalkWith(data.username);
			}
			$scope.$apply();
		});
		socket.on("personDisconnect", function(username){
			delete(aesKeys[username]);
			if($scope.messages[username]){
				$scope.messages[username].push({msg:username+" has disconnected"});
				$scope.$apply();
			}
		});
		socket.on("personConnect", function(username){
			delete(aesKeys[username]);
			if($scope.messages[username]){
				$scope.messages[username].push({msg:"reconnected with "+username});
				$scope.startTalkWith(username);
				$scope.$apply();
			}
		});
		socket.on("sendkey", function(data){
			if(aesKeys[data.username]){
				var usr = aesKeys[data.username];
				if(!usr['p2']){
					usr['p2'] = mycrypt.decrypt(data.key);
					usr['fin'] = usr['p1'] + usr['p2'];
				} else if(!usr['p1']){
					usr['p1'] = mycrypt.decrypt(data.key);
				}
			} else {
				if($scope.keys[data.username]){
					var rkey = genrand(64);
					var dec = mycrypt.decrypt(data.key);
					aesKeys[data.username] = {p1:dec,p2:rkey,fin:dec+rkey};
					othercrypt.setPublicKey($scope.keys[data.username]);
					socket.emit("sendkey", {username:data.username,key:othercrypt.encrypt(rkey)});
					if(!$scope.messages[data.username]){
						$scope.messages[data.username] = [];
					}
					$scope.messages[data.username].push({msg:data.username+" started talking to you"});
				} else {
					console.error("user not online");
				}
			}
		});
		$scope.startTalkWith = function(user, userPressed){
			if(userPressed){
				$scope.talkingTo = user;
			}
			if(!$scope.messages[user]){
				$scope.messages[user] = [];
			}
			if(!aesKeys[user]){
				if($scope.keys[user]){
					var rkey = genrand(64);
					aesKeys[user] = {};
					aesKeys[user]['p1'] = rkey;
					othercrypt.setPublicKey($scope.keys[user]);
					socket.emit("sendkey", {username:user,key:othercrypt.encrypt(rkey)});
					if(userPressed){
						$scope.messages[user].push({msg:"start talking with "+user});
					}
				} else {
					console.error("user not online");
					$scope.messages[user].push({msg:user+" not online"});
				}
			}
		};
		$scope.talkingTo = "";
		$scope.messages = {
			"": [{msg:"Click a username below. If there is none, that means no one is online"}]
		};
		$scope.keys = {};
		$scope.onChatInput = function(msg){
			if(aesKeys[$scope.talkingTo] && aesKeys[$scope.talkingTo].fin){
				if(msg){
					$scope.msg = "";
					if(!$scope.messages[$scope.talkingTo]){
						$scope.messages[$scope.talkingTo] = [];
					}
					$scope.messages[$scope.talkingTo].push({from:userInfo['username'],msg:msg}); // encrypt
					socket.emit("sendmsg",{username:$scope.talkingTo,msg:encrypt(msg, aesKeys[$scope.talkingTo].fin)});
				}
			} else {
				$scope.startTalkWith($scope.talkingTo);
			}
		}
	});