CREATE TABLE images(
   id INT NOT NULL AUTO_INCREMENT,
   encrypted TEXT NOT NULL,
   rand VARCHAR(17) NOT NULL,
   md5 VARCHAR(64) NOT NULL,
   user_id VARCHAR(10) NOT NULL,
   create_date DATETIME,
   PRIMARY KEY ( id )
);

CREATE TABLE comments(
   id INT NOT NULL AUTO_INCREMENT,
   message TEXT NOT NULL,
   image_id VARCHAR(10) NOT NULL,
   user_id VARCHAR(10) NOT NULL,
   create_date DATETIME,
   PRIMARY KEY ( id )
);

CREATE TABLE cookies(
   id INT NOT NULL AUTO_INCREMENT,
   user_id VARCHAR(10) NOT NULL,
   cookie VARCHAR(64) NOT NULL,
   create_date DATETIME,
   used_date DATETIME,
   PRIMARY KEY ( id )
);

CREATE TABLE users(
   id INT NOT NULL AUTO_INCREMENT,
   username VARCHAR(64) NOT NULL,
   email VARCHAR(128) NOT NULL,
   password TEXT NOT NULL,
   is_banned INT (2) NOT NULL,
   is_admin INT (2) NOT NULL,
   is_hidden INT (2) NOT NULL,
   create_date DATETIME,
   lastused_date DATETIME,
   token VARCHAR(17) NOT NULL,
   token_updated DATETIME,
   PRIMARY KEY ( id )
);